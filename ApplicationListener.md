# ApplicationListener执行流程分析

### spring中事件的实现

#### 第一步 构造一个事件监听类，实现ApplicationListener即可，ApplicationListener是支持泛型的,当然也可以不使用泛型
```
@Component
public class EnjoyApplicationListener implements ApplicationListener<EnjoyEvent> {

	@Override
	public void onApplicationEvent(EnjoyEvent event) {
		System.out.println("=========enjoyApplicationListener=============="+event.name);
	}
}
```
#### 第二步 构造 EnjoyEvent消息类
```
public class EnjoyEvent extends ApplicationEvent {
	public String name;

	/**
	 * Create a new {@code ApplicationEvent}.
	 *
	 * @param source the object on which the event initially occurred or with
	 *               which the event is associated (never {@code null})
	 */
	public EnjoyEvent(Object source,String name) {
		super(source);
		this.name=name;
	}
}
```
### 第三步调用
```
@Test
public  void test5(){
	ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
	ac.publishEvent(new EnjoyEvent(new Object(),"123123"));
}
```
spring ApplicationListener 大致实现原理总结：

	1spring在启动的时候会实例化一个事件管理类ApplicationEventMulticaster，这个类中维护了俩个set队列，用来存储实现了ApplicationListener的类和beanName
```
//这个list用来存储实例化后的信息
public final Set<ApplicationListener<?>> applicationListeners = new LinkedHashSet<>();
//用来存储beanName
public final Set<String> applicationListenerBeans = new LinkedHashSet<>();
```
	2将实现了ApplicationListener的接口的beanName添加到applicationListenerBeans
	
	3将实例化后的ApplicationListener实现添加到applicationListeners
	
	4然后调用```ac.publishEvent(new EnjoyEvent(xxx)))```,这个方法会循环这个维护了ApplicationListener的接口的List，调用onApplicationEvent()方法，实现消息通知

接下来我们围绕上面三个大步骤展开，在源代码中找到相应的逻辑

##### 事件管理类ApplicationEventMulticaster的实例化
```
public abstract class AbstractApplicationContext extends DefaultResourceLoader
		implements ConfigurableApplicationContext {
	//事件管理类
	private ApplicationEventMulticaster applicationEventMulticaster;	
		
	public void refresh() throws BeansException, IllegalStateException {
		synchronized (this.startupShutdownMonitor) {
			
			prepareRefresh();

			ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

			prepareBeanFactory(beanFactory);

			postProcessBeanFactory(beanFactory);

			invokeBeanFactoryPostProcessors(beanFactory);

			registerBeanPostProcessors(beanFactory);

			initMessageSource();

			//初始化事件管理类，添加到单例池中。观察者设计模式
			//主要看这个方法 new SimpleApplicationEventMulticaster(beanFactory)，然后添加到容器中
			initApplicationEventMulticaster();

			onRefresh();

			/**
			 * 事件管理类中注册事件类，这个方法在下面展开，先不用看，主要是把事件类注册到事件管理类中
			 */
			registerListeners();

			finishBeanFactoryInitialization(beanFactory);

			finishRefresh();
		}
	}
	/**
	 * 看这个方法，完成了事件管理类的注册，此时这个类还是一个空对象，没有任何事件监听类
	 */
	protected void initApplicationEventMulticaster() {
		ConfigurableListableBeanFactory beanFactory = getBeanFactory();
		if (beanFactory.containsLocalBean(APPLICATION_EVENT_MULTICASTER_BEAN_NAME)) {
			this.applicationEventMulticaster =
					beanFactory.getBean(APPLICATION_EVENT_MULTICASTER_BEAN_NAME, ApplicationEventMulticaster.class);
			if (logger.isTraceEnabled()) {
				logger.trace("Using ApplicationEventMulticaster [" + this.applicationEventMulticaster + "]");
			}
		}
		else {
			//new 一个 SimpleApplicationEventMulticaster对象
			this.applicationEventMulticaster = new SimpleApplicationEventMulticaster(beanFactory);
			//将事件管理类添加到单列容器中
			beanFactory.registerSingleton(APPLICATION_EVENT_MULTICASTER_BEAN_NAME, this.applicationEventMulticaster);
			if (logger.isTraceEnabled()) {
				logger.trace("No '" + APPLICATION_EVENT_MULTICASTER_BEAN_NAME + "' bean, using " +
						"[" + this.applicationEventMulticaster.getClass().getSimpleName() + "]");
			}
		}
	}
}
```

接下来看看事件管理类，顾名思义事件管理类是管理事件的，这只是一个接口定义了一些规范，不重要，可以不看。
```
public interface ApplicationEventMulticaster {
	//添加一个ApplicationListener
	void addApplicationListener(ApplicationListener<?> listener);
	void addApplicationListenerBean(String listenerBeanName);
	//删除一个ApplicationListener
	void removeApplicationListener(ApplicationListener<?> listener);
	void removeApplicationListenerBean(String listenerBeanName);
	void removeAllListeners();
    //广播消息
	void multicastEvent(ApplicationEvent event);
	void multicastEvent(ApplicationEvent event, @Nullable ResolvableType eventType);
}

```

接下来看看```SimpleApplicationEventMulticaster```，事件管理类的子类实现。主要对外提供发布消息的一些方法封装。
```
public class SimpleApplicationEventMulticaster extends AbstractApplicationEventMulticaster {

	@Nullable
	private Executor taskExecutor;

	@Nullable
	private ErrorHandler errorHandler;


	public SimpleApplicationEventMulticaster() {
	}

	public SimpleApplicationEventMulticaster(BeanFactory beanFactory) {
		setBeanFactory(beanFactory);
	}

	public void setTaskExecutor(@Nullable Executor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}

	@Nullable
	protected Executor getTaskExecutor() {
		return this.taskExecutor;
	}


	public void setErrorHandler(@Nullable ErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	@Nullable
	protected ErrorHandler getErrorHandler() {
		return this.errorHandler;
	}


	@Override
	public void multicastEvent(ApplicationEvent event) {
		multicastEvent(event, resolveDefaultEventType(event));
	}

	@Override
	public void multicastEvent(final ApplicationEvent event, @Nullable ResolvableType eventType) {
		ResolvableType type = (eventType != null ? eventType : resolveDefaultEventType(event));
		Executor executor = getTaskExecutor();
		for (ApplicationListener<?> listener : getApplicationListeners(event, type)) {
			if (executor != null) {
				executor.execute(() -> invokeListener(listener, event));
			}
			else {
				invokeListener(listener, event);
			}
		}
	}

	private ResolvableType resolveDefaultEventType(ApplicationEvent event) {
		return ResolvableType.forInstance(event);
	}


	protected void invokeListener(ApplicationListener<?> listener, ApplicationEvent event) {
		ErrorHandler errorHandler = getErrorHandler();
		if (errorHandler != null) {
			try {
				doInvokeListener(listener, event);
			}
			catch (Throwable err) {
				errorHandler.handleError(err);
			}
		}
		else {
			doInvokeListener(listener, event);
		}
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	private void doInvokeListener(ApplicationListener listener, ApplicationEvent event) {
		try {
			listener.onApplicationEvent(event);
		}
		catch (ClassCastException ex) {
			String msg = ex.getMessage();
			if (msg == null || matchesClassCastMessage(msg, event.getClass())) {
				// Possibly a lambda-defined listener which we could not resolve the generic event type for
				// -> let's suppress the exception and just log a debug message.
				Log logger = LogFactory.getLog(getClass());
				if (logger.isTraceEnabled()) {
					logger.trace("Non-matching event type for listener: " + listener, ex);
				}
			}
			else {
				throw ex;
			}
		}
	}

	private boolean matchesClassCastMessage(String classCastMessage, Class<?> eventClass) {
		// On Java 8, the message starts with the class name: "java.lang.String cannot be cast..."
		if (classCastMessage.startsWith(eventClass.getName())) {
			return true;
		}
		// On Java 11, the message starts with "class ..." a.k.a. Class.toString()
		if (classCastMessage.startsWith(eventClass.toString())) {
			return true;
		}
		// On Java 9, the message used to contain the module name: "java.base/java.lang.String cannot be cast..."
		int moduleSeparatorIndex = classCastMessage.indexOf('/');
		if (moduleSeparatorIndex != -1 && classCastMessage.startsWith(eventClass.getName(), moduleSeparatorIndex + 1)) {
			return true;
		}
		// Assuming an unrelated class cast failure...
		return false;
	}

}
```
上面看了一大圈也没找到事件管理类中的List容器啊，那么他到底在哪里呢，我们看到```SimpleApplicationEventMulticaster extends AbstractApplicationEventMulticaster```，我们打开
```AbstractApplicationEventMulticaster```，当中有个私有内部类，在这个私有内部类中定义了
```
//这个list用来存储实例化后的信息
public final Set<ApplicationListener<?>> applicationListeners = new LinkedHashSet<>();
 //用来存储beanName
public final Set<String> applicationListenerBeans = new LinkedHashSet<>();
```
这个applicationListenerBeans就是我们上文说的list容器
```
public abstract class AbstractApplicationEventMulticaster
		implements ApplicationEventMulticaster, BeanClassLoaderAware, BeanFactoryAware {
	
	//这个内部类当中保存了list容器
	private final DefaultListenerRetriever defaultRetriever = new DefaultListenerRetriever();
			
	final Map<ListenerCacheKey, CachedListenerRetriever> retrieverCache = new ConcurrentHashMap<>(64);
	
	@Override
	public void addApplicationListener(ApplicationListener<?> listener) {
		synchronized (this.defaultRetriever) {
			// Explicitly remove target for a proxy, if registered already,
			// in order to avoid double invocations of the same listener.
			Object singletonTarget = AopProxyUtils.getSingletonTarget(listener);
			if (singletonTarget instanceof ApplicationListener) {
				this.defaultRetriever.applicationListeners.remove(singletonTarget);
			}
			this.defaultRetriever.applicationListeners.add(listener);
			this.retrieverCache.clear();
		}
	}
	
	protected Collection<ApplicationListener<?>> getApplicationListeners() {
		synchronized (this.defaultRetriever) {
			return this.defaultRetriever.getApplicationListeners();
		}
	}
	

	private class DefaultListenerRetriever {
        
		//这个list用来存储实例化后的信息
		public final Set<ApplicationListener<?>> applicationListeners = new LinkedHashSet<>();
        //用来存储beanName
		public final Set<String> applicationListenerBeans = new LinkedHashSet<>();

		public Collection<ApplicationListener<?>> getApplicationListeners() {
			List<ApplicationListener<?>> allListeners = new ArrayList<>(
					this.applicationListeners.size() + this.applicationListenerBeans.size());
			allListeners.addAll(this.applicationListeners);
			if (!this.applicationListenerBeans.isEmpty()) {
				BeanFactory beanFactory = getBeanFactory();
				for (String listenerBeanName : this.applicationListenerBeans) {
					try {
						ApplicationListener<?> listener =
								beanFactory.getBean(listenerBeanName, ApplicationListener.class);
						if (!allListeners.contains(listener)) {
							allListeners.add(listener);
						}
					}
					catch (NoSuchBeanDefinitionException ex) {
						// Singleton listener instance (without backing bean definition) disappeared -
						// probably in the middle of the destruction phase
					}
				}
			}
			AnnotationAwareOrderComparator.sort(allListeners);
			return allListeners;
		}
	}
			
}
```

#### 将实现了ApplicationListener的接口名称添加到这个applicationListenerBeans容器当中

上面的内容你已经了解了事件管理类的大概结构，那么我们定义的ApplicationListener 是什么时候添加到容器当中的呢。我们主要看下面这个方法
```
/**
 * 事件管理类中注册事件类，这个方法在下面展开，先不用看，主要是把事件类注册到事件管理类中
 */
registerListeners();
```
```
public abstract class AbstractApplicationContext extends DefaultResourceLoader
		implements ConfigurableApplicationContext {
	//事件管理类
	private ApplicationEventMulticaster applicationEventMulticaster;
	protected void registerListeners() {
	
			//可以自己new一个ApplicationListener对象后添加到上下文
			for (ApplicationListener<?> listener : getApplicationListeners()) {
				getApplicationEventMulticaster().addApplicationListener(listener);
			}
	
			//主要看这段代码从beanFactory当中查找ApplicationListener的实现
			String[] listenerBeanNames = getBeanNamesForType(ApplicationListener.class, true, false);
			for (String listenerBeanName : listenerBeanNames) {
				//添加到ApplicationListener容器当中,这里添加的是beanName
				getApplicationEventMulticaster().addApplicationListenerBean(listenerBeanName);
			}
	
			// Publish early application events now that we finally have a multicaster...
			Set<ApplicationEvent> earlyEventsToProcess = this.earlyApplicationEvents;
			this.earlyApplicationEvents = null;
			if (!CollectionUtils.isEmpty(earlyEventsToProcess)) {
				for (ApplicationEvent earlyEvent : earlyEventsToProcess) {
					getApplicationEventMulticaster().multicastEvent(earlyEvent);
				}
			}
		}
}
```