# BeanDefinition的生成流程
spring 工厂中的bean都是 通过 BeanDefinition 来生成的
	
BeanDefinition 是一个接口，继承自 BeanMetadataElement 和 AttributeAccessor 接口。

## 1.BeanDefinition
```
BeanDefinition extends AttributeAccessor, BeanMetadataElement {
	//单列属性
	String SCOPE_SINGLETON = ConfigurableBeanFactory.SCOPE_SINGLETON;
	//原型属性
	String SCOPE_PROTOTYPE = ConfigurableBeanFactory.SCOPE_PROTOTYPE;
	//ROLE_APPLICATION 表示这个 Bean 是用户自己定义的
	int ROLE_APPLICATION = 0;
	//表示这个 Bean 是某些复杂配置的支撑部分
	int ROLE_SUPPORT = 1;
	//这是一个 Spring 内部的 Bean，通过 setRole/getRole 可以修改
	int ROLE_INFRASTRUCTURE = 2;
	//setParentName/getParentName 用来配置 parent 的名称，这个对应着 XML 中的 <bean parent=""> 配置
	void setParentName(@Nullable String parentName);
	@Nullable
	String getParentName();
	//配置 Bean 的 Class 全路径
	void setBeanClassName(@Nullable String beanClassName);
	String getBeanClassName();
	void setScope(@Nullable String scope);
	String getScope();
	void setLazyInit(boolean lazyInit);
	boolean isLazyInit();
	// 配置/获取 Bean 的依赖对象
	void setDependsOn(@Nullable String... dependsOn);
	String[] getDependsOn();
	void setAutowireCandidate(boolean autowireCandidate);
	boolean isAutowireCandidate();
	//配置/获取当前 Bean 是否为首选的 Bean
	void setPrimary(boolean primary);
	boolean isPrimary();
	void setFactoryBeanName(@Nullable String factoryBeanName);
	String getFactoryBeanName();
	void setFactoryMethodName(@Nullable String factoryMethodName);
	@Nullable
	String getFactoryMethodName();
	ConstructorArgumentValues getConstructorArgumentValues();
	default boolean hasConstructorArgumentValues() {
		return !getConstructorArgumentValues().isEmpty();
	}
	MutablePropertyValues getPropertyValues();
	default boolean hasPropertyValues() {
		return !getPropertyValues().isEmpty();
	}
	void setInitMethodName(@Nullable String initMethodName);
	String getInitMethodName();
	void setDestroyMethodName(@Nullable String destroyMethodName);
	String getDestroyMethodName();
	void setRole(int role);
	int getRole();
	void setDescription(@Nullable String description);
	String getDescription();
	ResolvableType getResolvableType();
	boolean isSingleton();
	boolean isPrototype();
	boolean isAbstract();
	String getResourceDescription();
	//如果当前 BeanDefinition 是一个代理对象，那么该方法可以用来返回原始的 BeanDefinition 。
	BeanDefinition getOriginatingBeanDefinition();
}


public interface AttributeAccessor {
    void setAttribute(String name, @Nullable Object value);
    @Nullable
    Object getAttribute(String name);
    @Nullable
    Object removeAttribute(String name);
    boolean hasAttribute(String name);
    String[] attributeNames();
}

public interface BeanMetadataElement {

	/**
	 * Return the configuration source {@code Object} for this metadata element
	 * (may be {@code null}).
	 */
	@Nullable
	default Object getSource() {
		return null;
	}

}
```
## 2.BeanDefinition 实现类
![](img/524917395-ade51bcd3cee1557_article732.png)