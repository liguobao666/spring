# 配置类初始化
```
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass({ RabbitTemplate.class, Channel.class })
@EnableConfigurationProperties(RabbitProperties.class)
@Import(RabbitAnnotationDrivenConfiguration.class)
public class RabbitAutoConfiguration {
	
	#完成了ConnectionFactory的初始化
	CachingConnectionFactory

}
```

# RabbitAnnotationDrivenConfiguration

完成 SimpleRabbitListenerContainerFactory DirectRabbitListenerContainerFactory初始化
```
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(EnableRabbit.class)
class RabbitAnnotationDrivenConfiguration {
	SimpleRabbitListenerContainerFactory
	DirectRabbitListenerContainerFactory
}
```

# EnableRabbit import 进来了RabbitListenerConfigurationSelector
```
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(RabbitListenerConfigurationSelector.class)
public @interface EnableRabbit {
	
}

# RabbitListenerConfigurationSelector
@Order
public class RabbitListenerConfigurationSelector implements DeferredImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		return new String[] { MultiRabbitBootstrapConfiguration.class.getName(),
				RabbitBootstrapConfiguration.class.getName()};
	}

}
```

# RabbitBootstrapConfiguration

初始化了RabbitListenerAnnotationBeanPostProcessor

```
public class RabbitBootstrapConfiguration implements ImportBeanDefinitionRegistrar {

	@Override
	public void registerBeanDefinitions(@Nullable AnnotationMetadata importingClassMetadata,
			BeanDefinitionRegistry registry) {

		if (!registry.containsBeanDefinition(
				RabbitListenerConfigUtils.RABBIT_LISTENER_ANNOTATION_PROCESSOR_BEAN_NAME)) {

			registry.registerBeanDefinition(RabbitListenerConfigUtils.RABBIT_LISTENER_ANNOTATION_PROCESSOR_BEAN_NAME,
					new RootBeanDefinition(RabbitListenerAnnotationBeanPostProcessor.class));
		}

		if (!registry.containsBeanDefinition(RabbitListenerConfigUtils.RABBIT_LISTENER_ENDPOINT_REGISTRY_BEAN_NAME)) {
			registry.registerBeanDefinition(RabbitListenerConfigUtils.RABBIT_LISTENER_ENDPOINT_REGISTRY_BEAN_NAME,
					new RootBeanDefinition(RabbitListenerEndpointRegistry.class));
		}
	}

}
```


```
public class RabbitListenerAnnotationBeanPostProcessor
		implements BeanPostProcessor, Ordered, BeanFactoryAware, BeanClassLoaderAware, EnvironmentAware,
		SmartInitializingSingleton {

	# 对RabbitListener注解查找和解析
	@Override
	public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
		Class<?> targetClass = AopUtils.getTargetClass(bean);
		final TypeMetadata metadata = this.typeCache.computeIfAbsent(targetClass, this::buildMetadata);
		for (ListenerMethod lm : metadata.listenerMethods) {
			for (RabbitListener rabbitListener : lm.annotations) {
				processAmqpListener(rabbitListener, lm.method, bean, beanName);
			}
		}
		if (metadata.handlerMethods.length > 0) {
			processMultiMethodListeners(metadata.classAnnotations, metadata.handlerMethods, bean, beanName);
		}
		return bean;
	}

}

```

# MessageListenerContainer 处理消息

```	
public class SimpleMessageListenerContainer extends AbstractMessageListenerContainer {
		
		
}
启动一个线程做了一些初始化工作
public class DirectMessageListenerContainer extends AbstractMessageListenerContainer {
	@Override
	public void start() {
		if (isRunning()) {
			return;
		}
		if (!this.initialized) {
			synchronized (this.lifecycleMonitor) {
				if (!this.initialized) {
					//第一步
					afterPropertiesSet();
				}
			}
		}
		try {
			logger.debug("Starting Rabbit listener container.");
			//第二步
			configureAdminIfNeeded();
			//第三步
			checkMismatchedQueues();
			
			doStart();
		}
		catch (Exception ex) {
			throw convertRabbitAccessException(ex);
		}
		finally {
			this.lazyLoad = false;
		}
	}
}
```

校验通过后将消费者consumer提交到线程池taskExecutors，主要逻辑分为3步：
（1）messageListener是ListenerContainerAware时的场景的处理，检查queueNames是否和messsgeListener的expectedQueueNames一致。

```
@Override
protected void doStart() {
	Assert.state(!this.consumerBatchEnabled || getMessageListener() instanceof BatchMessageListener
			|| getMessageListener() instanceof ChannelAwareBatchMessageListener,
			"When setting 'consumerBatchEnabled' to true, the listener must support batching");
	checkListenerContainerAware();
	//初始化active和running属性，并唤醒所有相关线程。
	super.doStart();
	//初始化consumers并提交
	synchronized (this.consumersMonitor) {
		if (this.consumers != null) {
			throw new IllegalStateException("A stopped container should not have consumers");
		}
		int newConsumers = initializeConsumers();
		if (this.consumers == null) {
			logger.info("Consumers were initialized and then cleared " +
					"(presumably the container was stopped concurrently)");
			return;
		}
		if (newConsumers <= 0) {
			if (logger.isInfoEnabled()) {
				logger.info("Consumers are already running");
			}
			return;
		}
		Set<AsyncMessageProcessingConsumer> processors = new HashSet<AsyncMessageProcessingConsumer>();
		for (BlockingQueueConsumer consumer : this.consumers) {
			AsyncMessageProcessingConsumer processor = new AsyncMessageProcessingConsumer(consumer);
			processors.add(processor);
			getTaskExecutor().execute(processor);
			if (getApplicationEventPublisher() != null) {
				getApplicationEventPublisher().publishEvent(new AsyncConsumerStartedEvent(this, consumer));
			}
		}
		waitForConsumersToStart(processors);
	}
}
```


```
private final class AsyncMessageProcessingConsumer implements Runnable {
	@Override 
	public void run() {
		//1.active校验
		if (!isActive()) {
			return;
		}

		boolean aborted = false;

		this.consumer.setLocallyTransacted(isChannelLocallyTransacted());

		String routingLookupKey = getRoutingLookupKey();
		if (routingLookupKey != null) {
			SimpleResourceHolder.bind(getRoutingConnectionFactory(), routingLookupKey); // NOSONAR both never null
		}
		
		//3. queue数量校验
		if (this.consumer.getQueueCount() < 1) {
			if (logger.isDebugEnabled()) {
				logger.debug("Consumer stopping; no queues for " + this.consumer);
			}
			SimpleMessageListenerContainer.this.cancellationLock.release(this.consumer);
			if (getApplicationEventPublisher() != null) {
				getApplicationEventPublisher().publishEvent(
						new AsyncConsumerStoppedEvent(SimpleMessageListenerContainer.this, this.consumer));
			}
			this.start.countDown();
			return;
		}

		try {
			//4 consumer启动
			initialize();
			//接受消息
			while (isActive(this.consumer) || this.consumer.hasDelivery() || !this.consumer.cancelled()) {
				mainLoop();
			}
		}
		catch (InterruptedException e) {
			logger.debug("Consumer thread interrupted, processing stopped.");
			Thread.currentThread().interrupt();
			aborted = true;
			publishConsumerFailedEvent("Consumer thread interrupted, processing stopped", true, e);
		}
		catch (QueuesNotAvailableException ex) {
			logger.error("Consumer threw missing queues exception, fatal=" + isMissingQueuesFatal(), ex);
			if (isMissingQueuesFatal()) {
				this.startupException = ex;
				// Fatal, but no point re-throwing, so just abort.
				aborted = true;
			}
			publishConsumerFailedEvent("Consumer queue(s) not available", aborted, ex);
		}
		catch (FatalListenerStartupException ex) {
			logger.error("Consumer received fatal exception on startup", ex);
			this.startupException = ex;
			// Fatal, but no point re-throwing, so just abort.
			aborted = true;
			publishConsumerFailedEvent("Consumer received fatal exception on startup", true, ex);
		}
		catch (FatalListenerExecutionException ex) { // NOSONAR exception as flow control
			logger.error("Consumer received fatal exception during processing", ex);
			// Fatal, but no point re-throwing, so just abort.
			aborted = true;
			publishConsumerFailedEvent("Consumer received fatal exception during processing", true, ex);
		}
		catch (PossibleAuthenticationFailureException ex) {
			logger.error("Consumer received fatal=" + isPossibleAuthenticationFailureFatal() +
					" exception during processing", ex);
			if (isPossibleAuthenticationFailureFatal()) {
				this.startupException =
						new FatalListenerStartupException("Authentication failure",
								new AmqpAuthenticationException(ex));
				// Fatal, but no point re-throwing, so just abort.
				aborted = true;
			}
			publishConsumerFailedEvent("Consumer received PossibleAuthenticationFailure during startup", aborted, ex);
		}
		catch (ShutdownSignalException e) {
			if (RabbitUtils.isNormalShutdown(e)) {
				if (logger.isDebugEnabled()) {
					logger.debug("Consumer received Shutdown Signal, processing stopped: " + e.getMessage());
				}
			}
			else {
				logConsumerException(e);
			}
		}
		catch (AmqpIOException e) {
			if (e.getCause() instanceof IOException && e.getCause().getCause() instanceof ShutdownSignalException
					&& e.getCause().getCause().getMessage().contains("in exclusive use")) {
				getExclusiveConsumerExceptionLogger().log(logger,
						"Exclusive consumer failure", e.getCause().getCause());
				publishConsumerFailedEvent("Consumer raised exception, attempting restart", false, e);
			}
			else {
				logConsumerException(e);
			}
		}
		catch (Error e) { //NOSONAR
			logger.error("Consumer thread error, thread abort.", e);
			publishConsumerFailedEvent("Consumer threw an Error", true, e);
			getJavaLangErrorHandler().handle(e);
			aborted = true;
		}
		catch (Throwable t) { //NOSONAR
			// by now, it must be an exception
			if (isActive()) {
				logConsumerException(t);
			}
		}
		finally {
			if (getTransactionManager() != null) {
				ConsumerChannelRegistry.unRegisterConsumerChannel();
			}
		}

		// In all cases count down to allow container to progress beyond startup
		this.start.countDown();

		killOrRestart(aborted);

		if (routingLookupKey != null) {
			SimpleResourceHolder.unbind(getRoutingConnectionFactory()); // NOSONAR never null here
		}
	}
	
}
```