# spring中的属性注入

spring 中的属性注入主要依靠 AutowiredAnnotationBeanPostProcessor 和CommonAnnotationBeanPostProcessor 这俩个类来完成
#### AutowiredAnnotationBeanPostProcessor 
```
public class AutowiredAnnotationBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
		implements MergedBeanDefinitionPostProcessor, PriorityOrdered, BeanFactoryAware {			
}	
```
#### CommonAnnotationBeanPostProcessor
```
public class CommonAnnotationBeanPostProcessor extends InitDestroyAnnotationBeanPostProcessor
		implements InstantiationAwareBeanPostProcessor, BeanFactoryAware, Serializable {		
}	
```
### 调用流程

```
//入口方法
@Test
public  void test5(){
	ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml"); //ClassPathXmlApplicationContext(从类路径下加载xml的Application容器)是org.springframework.context.ApplicationContext的实现类
	ac.publishEvent(new EnjoyEvent(new Object(),"123123"));
}

//调用流程
public ClassPathXmlApplicationContext(String configLocation) 
	public ClassPathXmlApplicationContext(String[] configLocations, boolean refresh, @Nullable ApplicationContext parent)	
		public void refresh() throws BeansException, IllegalStateException 	
			protected void finishBeanFactoryInitialization(ConfigurableListableBeanFactory beanFactory)
				public void preInstantiateSingletons()
					public Object getBean(String name)
						protected <T> T doGetBean()
							protected Object createBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
								protected Object doCreateBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
```


### doCreateBean()方法中完成了bean的实例化,完成了属性的收集和注入
这个方法干的事情比较多，你只需要关注下面来个方法即可


	applyMergedBeanDefinitionPostProcessors();完成了属性的收集

	populateBean(beanName, mbd, instanceWrapper);完成了属性的注入
	
```
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory
		implements AutowireCapableBeanFactory {
protected Object doCreateBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
			throws BeanCreationException {
        .......这里是伪代码.....
		// Instantiate the bean.
		//把实例化完成的对象做了一个包装
		BeanWrapper instanceWrapper = null;
		/**
		 * 完成了推断构造方法和实例化的事情,创建了对象这里还不是一个bean,重要程度5
		 * 这个放法完成对构造函数的调用，但是这里的对象仅仅是一个空对象
		 */
		instanceWrapper = createBeanInstance(beanName, mbd, args);
		//拿到真正创建的实例
		Object bean = instanceWrapper.getWrappedInstance();
		Class<?> beanType = instanceWrapper.getWrappedClass();
		

		/**
		 * CommonAnnotationBeanPostProcessor 支持了@PostContruct @PreDestroy @Resource
		 * AutowiredAnnotationBeanPostProcessor 支持@Autowired @Value
		 * BeanPostProcessor接口的典型运用，这里要理解这个接口
		 * 重要程度5，主要功能收集注解
		 */
		applyMergedBeanDefinitionPostProcessors(mbd, beanType, beanName);

		//缓存一个对象解决循环依赖
		//doCreateBean-2
		// 给earlySingletonExposure这个布尔类型的变量赋值；这个变量的意义是——是否支持（开启了）循环依赖；
		// 如果返回true则spring会做一些特殊的操作来完成循环依赖；
		boolean earlySingletonExposure = (mbd.isSingleton() && this.allowCircularReferences &&
				isSingletonCurrentlyInCreation(beanName));
		if (earlySingletonExposure) {//是否开启了循环依赖，提前暴露一个工厂出去
			if (logger.isTraceEnabled()) {
				logger.trace("Eagerly caching bean '" + beanName +
						"' to allow for resolving potential circular references");
			}
			//这里做了一系列的缓存操作的增删
			addSingletonFactory(beanName, () -> getEarlyBeanReference(beanName, mbd, bean));
		}

		// Initialize the bean instance.
		Object exposedObject = bean;
		
		//完成属性填充，ioc,di的核心方法
		populateBean(beanName, mbd, instanceWrapper);
		/**
		 * 1 调用Aware接口
		 * 2 调用@PostConstruct注解的方法，这里是通过BeanPostProcessor实现的该功能
		 * 3 调用了实现InitiaLizingBean接口的类中的afterPropertiesSet方法调用initMethod中配置的方法
		 * 4 代理实例创建，用BeanPostProcessor接口来完成代理实例的生成
		 */
		exposedObject = initializeBean(beanName, exposedObject, mbd);
		
		return exposedObject;
	}
}
```

##  注解收集入口 applyMergedBeanDefinitionPostProcessors(mbd, beanType, beanName)
 doCreateBean() 方法中的applyMergedBeanDefinitionPostProcessors()方法完成了注解的收集,注意看
 applyMergedBeanDefinitionPostProcessors()方法，这里面循环BeanPostProcessor接口的实现
```
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory
		implements AutowireCapableBeanFactory {
	protected Object doCreateBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
				throws BeanCreationException {
			/**
			* CommonAnnotationBeanPostProcessor 支持了@PostContruct @PreDestroy @Resource
			* AutowiredAnnotationBeanPostProcessor 支持@Autowired @Value
			* BeanPostProcessor接口的典型运用，这里要理解这个接口
			* 对类中注解的装配过程
			* 重要程度5，主要功能收集注解
			*/
			applyMergedBeanDefinitionPostProcessors(mbd, beanType, beanName);
			.....
			.....
			//完成属性填充，ioc,di的核心方法
			populateBean(beanName, mbd, instanceWrapper);

	}

	//完成注解属性的收集和调用
	protected void applyMergedBeanDefinitionPostProcessors(RootBeanDefinition mbd, Class<?> beanType, String beanName) {
		for (BeanPostProcessor bp : getBeanPostProcessors()) {
			if (bp instanceof MergedBeanDefinitionPostProcessor) {
				//主要看这里
				MergedBeanDefinitionPostProcessor bdp = (MergedBeanDefinitionPostProcessor) bp;
				bdp.postProcessMergedBeanDefinition(mbd, beanType, beanName);
			}
		}
	}
}
```

#### 注解收集bdp.postProcessMergedBeanDefinition(mbd, beanType, beanName);
调用for循环当中bdp.postProcessMergedBeanDefinition(mbd, beanType, beanName)方法，依次执行如AutowiredAnnotationBeanPostProcessor 和 CommonAnnotationBeanPostProcessor
中的postProcessMergedBeanDefinition()方法完成注解收集，将注解封装到InjectionMetadata当中，然后保存到一个Map当中，key为beanName,value 为 InjectionMetadata
```
private final Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(256);
```

##### AutowiredAnnotationBeanPostProcessor
收集@Autowired @Value注解的收集
```
public class AutowiredAnnotationBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
		implements MergedBeanDefinitionPostProcessor, PriorityOrdered, BeanFactoryAware {
			 
	private final Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(256);
	@Override
	public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
		//收集@Autowired @Value注解的收集，包装成InjectionMetadata对象
		InjectionMetadata metadata = findAutowiringMetadata(beanName, beanType, null);
		metadata.checkConfigMembers(beanDefinition);
	}
	//
	private class AutowiredFieldElement extends InjectionMetadata.InjectedElement {
		......
	}
	private class AutowiredMethodElement extends InjectionMetadata.InjectedElement {
		......
	}
}	
```
我们看这个方法,首先从缓存中拿数据，如果缓存中没有就去收集注解的属性和方法，主要收集逻辑在buildAutowiringMetadata(clazz)方法，这个方法返回InjectionMetadata类型，然后调用
this.injectionMetadataCache.put(cacheKey, metadata);将信息缓存到injectionMetadataCache当中
```
public class AutowiredAnnotationBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
		implements MergedBeanDefinitionPostProcessor, PriorityOrdered, BeanFactoryAware {
			
	private final Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(256);
	
	private InjectionMetadata findAutowiringMetadata(String beanName, Class<?> clazz, @Nullable PropertyValues pvs) {
		// Fall back to class name as cache key, for backwards compatibility with custom callers.
		String cacheKey = (StringUtils.hasLength(beanName) ? beanName : clazz.getName());
		// Quick check on the concurrent map first, with minimal locking.
		//第一次从缓存中拿
		InjectionMetadata metadata = this.injectionMetadataCache.get(cacheKey);
		//如果缓存中没有就去收集
		if (InjectionMetadata.needsRefresh(metadata, clazz)) {
			synchronized (this.injectionMetadataCache) {
				//第二次从缓存中拿，双重检索
				metadata = this.injectionMetadataCache.get(cacheKey);
				if (InjectionMetadata.needsRefresh(metadata, clazz)) {
					if (metadata != null) {
						metadata.clear(pvs);
					}
					//看这个方法，这里面收集
					metadata = buildAutowiringMetadata(clazz);
					//添加到属性缓存中
					this.injectionMetadataCache.put(cacheKey, metadata);
				}
			}
		}
		return metadata;
	}
}
```
我们进入buildAutowiringMetadata()方法,这个方法比较长，你只需要关注 do while循环当中的内容，这里分为俩部分，第一部分为属性的收集，第二部分为方法的收集。
属性收集将属性封装成AutowiredFieldElement对象，然后添加到currElements当中。

方法收集
```
private InjectionMetadata buildAutowiringMetadata(final Class<?> clazz) {
		if (!AnnotationUtils.isCandidateClass(clazz, this.autowiredAnnotationTypes)) {
			return InjectionMetadata.EMPTY;
		}

		List<InjectionMetadata.InjectedElement> elements = new ArrayList<>();
		Class<?> targetClass = clazz;

		do {
			final List<InjectionMetadata.InjectedElement> currElements = new ArrayList<>();
            //寻找field 上面的@Autowired 注解并封装成对象
			ReflectionUtils.doWithLocalFields(targetClass, field -> {
				//拿到属性上的@Autowired注解
				MergedAnnotation<?> ann = findAutowiredAnnotation(field);
				if (ann != null) {
					//我们可以看到静态的属性不支持注入
					if (Modifier.isStatic(field.getModifiers())) {
						if (logger.isInfoEnabled()) {
							logger.info("Autowired annotation is not supported on static fields: " + field);
						}
						return;//理解这里的return方法，是结束lamdba表达式，并不是退出doWithLocalFields()中的循环
					}
					boolean required = determineRequiredStatus(ann);
					currElements.add(new AutowiredFieldElement(field, required));
				}
			});
			/****************************************************************************************/
			//寻找method 上面的@Autowired 注解并封装成对象
			ReflectionUtils.doWithLocalMethods(targetClass, method -> {
				Method bridgedMethod = BridgeMethodResolver.findBridgedMethod(method);
				if (!BridgeMethodResolver.isVisibilityBridgeMethodPair(method, bridgedMethod)) {
					return;
				}
				MergedAnnotation<?> ann = findAutowiredAnnotation(bridgedMethod);
				if (ann != null && method.equals(ClassUtils.getMostSpecificMethod(method, clazz))) {
					//同样静态的方法不支持注入
					if (Modifier.isStatic(method.getModifiers())) {
						if (logger.isInfoEnabled()) {
							logger.info("Autowired annotation is not supported on static methods: " + method);
						}
						return;
					}
					if (method.getParameterCount() == 0) {
						if (logger.isInfoEnabled()) {
							logger.info("Autowired annotation should only be used on methods with parameters: " +
									method);
						}
					}
					boolean required = determineRequiredStatus(ann);
					PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
					currElements.add(new AutowiredMethodElement(method, required, pd));
				}
			});

			elements.addAll(0, currElements);
			targetClass = targetClass.getSuperclass();
		}
		while (targetClass != null && targetClass != Object.class);
        //返回一个InjectionMetadata对象
		return InjectionMetadata.forElements(elements, clazz);
}
```

属性的收集doWithLocalFields()方法循环类中的属性，然后调用fc.doWith(field)(方法中的lamdba表达式)，在lamdba表达式中，拿到属性上的注解，然
后将属性包装成AutowiredFieldElement，然后添加到 currElements当中，自此属性收集完毕。当然方法的收集也是类似。最后调用InjectionMetadata.forElements(elements, clazz);
返回一个InjectionMetadata对象。
```
final List<InjectionMetadata.InjectedElement> currElements = new ArrayList<>();
//寻找field 上面的@Autowired 注解并封装成对象
ReflectionUtils.doWithLocalFields(targetClass, field -> {
	//拿到属性上的@Autowired @Value注解
	MergedAnnotation<?> ann = findAutowiredAnnotation(field);
	if (ann != null) {
		if (Modifier.isStatic(field.getModifiers())) {
			if (logger.isInfoEnabled()) {
				logger.info("Autowired annotation is not supported on static fields: " + field);
			}
			return;//理解这里的return方法，是结束lamdba表达式，并不是调出doWithLocalFields()中的循环
		}
		boolean required = determineRequiredStatus(ann);
		currElements.add(new AutowiredFieldElement(field, required));
	}
});


public static void doWithLocalFields(Class<?> clazz, FieldCallback fc) {
	//通过反射拿到类中的属性
	for (Field field : getDeclaredFields(clazz)) {
		try {
			fc.doWith(field);
		}
		catch (IllegalAccessException ex) {
			throw new IllegalStateException("Not allowed to access field '" + field.getName() + "': " + ex);
		}
	}
}
```

##### CommonAnnotationBeanPostProcessor
完成@Resource注解的收集


```
public class CommonAnnotationBeanPostProcessor extends InitDestroyAnnotationBeanPostProcessor
		implements InstantiationAwareBeanPostProcessor, BeanFactoryAware, Serializable {
	缓存信息
	private final transient Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(256);
	
	@Override
	public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
		super.postProcessMergedBeanDefinition(beanDefinition, beanType, beanName);
		//完成注解的收集
		InjectionMetadata metadata = findResourceMetadata(beanName, beanType, null);
		metadata.checkConfigMembers(beanDefinition);
	}
}	
```
查看findResourceMetadata(beanName, beanType, null)方法，与AutowiredAnnotationBeanPostProcessor中的方法大致相同
```
private InjectionMetadata findResourceMetadata(String beanName, final Class<?> clazz, @Nullable PropertyValues pvs) {
	// Fall back to class name as cache key, for backwards compatibility with custom callers.
	String cacheKey = (StringUtils.hasLength(beanName) ? beanName : clazz.getName());
	// Quick check on the concurrent map first, with minimal locking.
	InjectionMetadata metadata = this.injectionMetadataCache.get(cacheKey);
	if (InjectionMetadata.needsRefresh(metadata, clazz)) {
		synchronized (this.injectionMetadataCache) {
			metadata = this.injectionMetadataCache.get(cacheKey);
			if (InjectionMetadata.needsRefresh(metadata, clazz)) {
				if (metadata != null) {
					metadata.clear(pvs);
				}
				//看这里,属性收集
				metadata = buildResourceMetadata(clazz);
				//添加到缓存
				this.injectionMetadataCache.put(cacheKey, metadata);
			}
		}
	}
	return metadata;
}
```

查看buildResourceMetadata(final Class<?> clazz)方法，也是一个do while循环，具体看方法注释
```
private InjectionMetadata buildResourceMetadata(final Class<?> clazz) {
		if (!AnnotationUtils.isCandidateClass(clazz, resourceAnnotationTypes)) {
			return InjectionMetadata.EMPTY;
		}
        
		List<InjectionMetadata.InjectedElement> elements = new ArrayList<>();
		Class<?> targetClass = clazz;

		do {
			//存储方法和属性的List集合
			final List<InjectionMetadata.InjectedElement> currElements = new ArrayList<>();
            //属性的收集
			ReflectionUtils.doWithLocalFields(targetClass, field -> {
				if (webServiceRefClass != null && field.isAnnotationPresent(webServiceRefClass)) {
					if (Modifier.isStatic(field.getModifiers())) {
						throw new IllegalStateException("@WebServiceRef annotation is not supported on static fields");
					}
					currElements.add(new WebServiceRefElement(field, field, null));
				}
				else if (ejbClass != null && field.isAnnotationPresent(ejbClass)) {
					if (Modifier.isStatic(field.getModifiers())) {
						throw new IllegalStateException("@EJB annotation is not supported on static fields");
					}
					currElements.add(new EjbRefElement(field, field, null));
				}
			    //看这里比较直观直判断属性是否是Resource类型
				else if (field.isAnnotationPresent(Resource.class)) {
					if (Modifier.isStatic(field.getModifiers())) {
						throw new IllegalStateException("@Resource annotation is not supported on static fields");
					}
					//添加到currElements
					if (!this.ignoredResourceTypes.contains(field.getType().getName())) {
						currElements.add(new ResourceElement(field, field, null));
					}
				}
			});
            //方法的收集
			ReflectionUtils.doWithLocalMethods(targetClass, method -> {
				Method bridgedMethod = BridgeMethodResolver.findBridgedMethod(method);
				if (!BridgeMethodResolver.isVisibilityBridgeMethodPair(method, bridgedMethod)) {
					return;
				}
				if (method.equals(ClassUtils.getMostSpecificMethod(method, clazz))) {
					if (webServiceRefClass != null && bridgedMethod.isAnnotationPresent(webServiceRefClass)) {
						if (Modifier.isStatic(method.getModifiers())) {
							throw new IllegalStateException("@WebServiceRef annotation is not supported on static methods");
						}
						if (method.getParameterCount() != 1) {
							throw new IllegalStateException("@WebServiceRef annotation requires a single-arg method: " + method);
						}
						PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
						currElements.add(new WebServiceRefElement(method, bridgedMethod, pd));
					}
					else if (ejbClass != null && bridgedMethod.isAnnotationPresent(ejbClass)) {
						if (Modifier.isStatic(method.getModifiers())) {
							throw new IllegalStateException("@EJB annotation is not supported on static methods");
						}
						if (method.getParameterCount() != 1) {
							throw new IllegalStateException("@EJB annotation requires a single-arg method: " + method);
						}
						PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
						currElements.add(new EjbRefElement(method, bridgedMethod, pd));
					}
					//同样判断注解类型
					else if (bridgedMethod.isAnnotationPresent(Resource.class)) {
						if (Modifier.isStatic(method.getModifiers())) {
							throw new IllegalStateException("@Resource annotation is not supported on static methods");
						}
						Class<?>[] paramTypes = method.getParameterTypes();
						if (paramTypes.length != 1) {
							throw new IllegalStateException("@Resource annotation requires a single-arg method: " + method);
						}
						//添加到currElements
						if (!this.ignoredResourceTypes.contains(paramTypes[0].getName())) {
							PropertyDescriptor pd = BeanUtils.findPropertyForMethod(bridgedMethod, clazz);
							currElements.add(new ResourceElement(method, bridgedMethod, pd));
						}
					}
				}
			});

			elements.addAll(0, currElements);
			targetClass = targetClass.getSuperclass();
		}
		while (targetClass != null && targetClass != Object.class);

		return InjectionMetadata.forElements(elements, clazz);
	}
```

## 属性的注入 populateBean(beanName, mbd, instanceWrapper)
接下来我们进入populateBean()方法，这里会循环实现了BeanPostProcessor的所有接口，其中就包括 AutowiredAnnotationBeanPostProcessor和CommonAnnotationBeanPostProcessor,然后调用
ibp.postProcessProperties(pvs, bw.getWrappedInstance(), beanName) 方法
```
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory
		implements AutowireCapableBeanFactory {
	protected void populateBean(String beanName, RootBeanDefinition mbd, @Nullable BeanWrapper bw) {
		......
		//循环BeanPostProcessor 完成不同注解的属性注入
		for (BeanPostProcessor bp : getBeanPostProcessors()) {
			if (bp instanceof InstantiationAwareBeanPostProcessor) {
				InstantiationAwareBeanPostProcessor ibp = (InstantiationAwareBeanPostProcessor) bp;
				//依赖注入过程
				PropertyValues pvsToUse = ibp.postProcessProperties(pvs, bw.getWrappedInstance(), beanName);
				if (pvsToUse == null) {
					if (filteredPds == null) {
						filteredPds = filterPropertyDescriptorsForDependencyCheck(bw, mbd.allowCaching);
					}
					//老版本用这个完成依赖注入过程
					pvsToUse = ibp.postProcessPropertyValues(pvs, filteredPds, bw.getWrappedInstance(), beanName);
					if (pvsToUse == null) {
						return;
					}
				}
				pvs = pvsToUse;
			}
		}
		......
	}
}
```
进入postProcessProperties()方法，调用findAutowiringMetadata()方法从injectionMetadataCache缓存中拿到属性和方法的包装InjectionMetadata,然后调用
metadata.inject(bean, beanName, pvs)完成属性的注入
```
public class AutowiredAnnotationBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
		implements MergedBeanDefinitionPostProcessor, PriorityOrdered, BeanFactoryAware {
	//方法属性缓存
	private final Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(256);
	@Override
	public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) {
		//从缓存里拿到加了注解的属性和方法
		InjectionMetadata metadata = findAutowiringMetadata(beanName, bean.getClass(), pvs);
		try {
			//属性注入
			metadata.inject(bean, beanName, pvs);
		}
		catch (BeanCreationException ex) {
			throw ex;
		}
		catch (Throwable ex) {
			throw new BeanCreationException(beanName, "Injection of autowired dependencies failed", ex);
		}
		return pvs;
	}
}
```
进入metadata.inject(bean, beanName, pvs)方法，拿到InjectionMetadata中的injectedElements属性，这个里面封装了属性和方法，然后循环injectedElements，通过反射分别完成属性和方法的注入
```
public void inject(Object target, @Nullable String beanName, @Nullable PropertyValues pvs) throws Throwable {
	Collection<InjectedElement> checkedElements = this.checkedElements;
	Collection<InjectedElement> elementsToIterate =
			(checkedElements != null ? checkedElements : this.injectedElements);
	if (!elementsToIterate.isEmpty()) {
		for (InjectedElement element : elementsToIterate) {
			//有注解的属性和方法调用
			element.inject(target, beanName, pvs);
		}
	}
}
```
对于AutowiredAnnotationBeanPostProcessor类型来说，element.inject(target, beanName, pvs)方法会调用到下面俩个方法
```
public class AutowiredAnnotationBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
		implements MergedBeanDefinitionPostProcessor, PriorityOrdered, BeanFactoryAware {
	//内部类
	private class AutowiredFieldElement extends InjectionMetadata.InjectedElement {
		@Override
		protected void inject(Object bean, @Nullable String beanName, @Nullable PropertyValues pvs) throws Throwable {
			Field field = (Field) this.member;
			Object value;
			if (this.cached) {
				try {
					value = resolvedCachedArgument(beanName, this.cachedFieldValue);
				}
				catch (NoSuchBeanDefinitionException ex) {
					// Unexpected removal of target bean for cached argument -> re-resolve
					value = resolveFieldValue(field, bean, beanName);
				}
			}
			else {
				value = resolveFieldValue(field, bean, beanName);
			}
			if (value != null) {
				ReflectionUtils.makeAccessible(field);
				//反射调用
				field.set(bean, value);
			}
		}
	}
	//内部类
	private class AutowiredMethodElement extends InjectionMetadata.InjectedElement {
		@Override
		protected void inject(Object bean, @Nullable String beanName, @Nullable PropertyValues pvs) throws Throwable {
			if (checkPropertySkipping(pvs)) {
				return;
			}
			Method method = (Method) this.member;
			Object[] arguments;
			if (this.cached) {
				try {
					arguments = resolveCachedArguments(beanName);
				}
				catch (NoSuchBeanDefinitionException ex) {
					// Unexpected removal of target bean for cached argument -> re-resolve
					arguments = resolveMethodArguments(method, bean, beanName);
				}
			}
			else {
				arguments = resolveMethodArguments(method, bean, beanName);
			}
			if (arguments != null) {
				try {
					ReflectionUtils.makeAccessible(method);
					//反射调用
					method.invoke(bean, arguments);
				}
				catch (InvocationTargetException ex) {
					throw ex.getTargetException();
				}
			}
		}
     }
			
}
```
对于CommonAnnotationBeanPostProcessor，会调用到InjectionMetadata中的InjectedElement中的inject()方法
```
public class InjectionMetadata {
	//内部类
	public abstract static class InjectedElement {
		protected void inject(Object target, @Nullable String requestingBeanName, @Nullable PropertyValues pvs)
				throws Throwable {

			if (this.isField) {
				Field field = (Field) this.member;
				ReflectionUtils.makeAccessible(field);
				//属性注入
				field.set(target, getResourceToInject(target, requestingBeanName));
			}
			else {
				if (checkPropertySkipping(pvs)) {
					return;
				}
				try {
					Method method = (Method) this.member;
					ReflectionUtils.makeAccessible(method);
					//方法注入
					method.invoke(target, getResourceToInject(target, requestingBeanName));
				}
				catch (InvocationTargetException ex) {
					throw ex.getTargetException();
				}
			}
		}
	}
}
```
自此属性注入完毕，这里只是一个大致过程，注入细节还有很多，以后再来补充。