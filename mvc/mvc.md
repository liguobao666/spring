
# spi 技术
	取代web.xml用到的技术是tomcat的spi技术
	
	tomcat 会加载spring-web下面的javax.servlet.ServletContainerInitializer这个文件
![](mvc_files/1.png)
	
## SpringServletContainerInitializer

@HandlesTypes 注解会收集实现了WebApplicationInitializer的接口的类，
tomcat调用onStartup()方法，将收集到的类当作入参
```
@HandlesTypes(WebApplicationInitializer.class)
public class SpringServletContainerInitializer implements ServletContainerInitializer {
	@Override
	public void onStartup(@Nullable Set<Class<?>> webAppInitializerClasses, ServletContext servletContext)
			throws ServletException {

		List<WebApplicationInitializer> initializers = new LinkedList<>();

		if (webAppInitializerClasses != null) {
			for (Class<?> waiClass : webAppInitializerClasses) {
				// Be defensive: Some servlet containers provide us with invalid classes,
				// no matter what @HandlesTypes says...
				if (!waiClass.isInterface() && !Modifier.isAbstract(waiClass.getModifiers()) &&
						WebApplicationInitializer.class.isAssignableFrom(waiClass)) {
					try {
						initializers.add((WebApplicationInitializer)
								ReflectionUtils.accessibleConstructor(waiClass).newInstance());
					}
					catch (Throwable ex) {
						throw new ServletException("Failed to instantiate WebApplicationInitializer class", ex);
					}
				}
			}
		}

		if (initializers.isEmpty()) {
			servletContext.log("No Spring WebApplicationInitializer types detected on classpath");
			return;
		}

		servletContext.log(initializers.size() + " Spring WebApplicationInitializers detected on classpath");
		AnnotationAwareOrderComparator.sort(initializers);
		//@1
		for (WebApplicationInitializer initializer : initializers) {
			//在这个方法里就完成了Spring mvc容器的启动
			initializer.onStartup(servletContext);
		}
	}
		
}

```


## initializer.onStartup(servletContext);
```
//@1
for (WebApplicationInitializer initializer : initializers) {
	//在这个方法里就完成了Spring mvc容器的启动
	initializer.onStartup(servletContext);
}
```
TOMCAt 要启动容器必须要实现WebApplicationInitializer方法

在initializer.onStartup(servletContext)方法中完成了Spring Mvc, spring容器的初始化工作

```
public abstract class AbstractDispatcherServletInitializer extends AbstractContextLoaderInitializer {

	/**
	 * The default servlet name. Can be customized by overriding {@link #getServletName}.
	 */
	public static final String DEFAULT_SERVLET_NAME = "dispatcher";


	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		//创建跟上下文(Spring容器的启动),创建servletListener
		super.onStartup(servletContext);
		//创建mvc上下文,注册DispatcherServlet
		registerDispatcherServlet(servletContext);
	}
}

```

### @EnableMvc注解
完成一些初始化工作
```
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(DelegatingWebMvcConfiguration.class)
public @interface EnableWebMvc {
}
```

# Spring MVC请求如何到达Controller

1 HandlerMethod的映射关系
2 参数解析
3 视图响应

初始化
```
public class DispatcherServlet extends FrameworkServlet {
	@Override
	protected void onRefresh(ApplicationContext context) {
		initStrategies(context);
	}
	protected void initStrategies(ApplicationContext context) {
			initMultipartResolver(context);
			initLocaleResolver(context);
			initThemeResolver(context);
			//HandlerMapping 映射关系建立
			initHandlerMappings(context);
			//HandlerAdapters 负责去调用具体逻辑，调用controller，参数解析，返回值参数解析等
			initHandlerAdapters(context);
			initHandlerExceptionResolvers(context);
			initRequestToViewNameTranslator(context);
			//响应试图
			initViewResolvers(context);
			initFlashMapManager(context);
	}
}
```

## 七个handlerMapping的作用

	1:RequestMappingHandlerMapping
	2:SimpleUrlHandlerMapping
	3:BeanNameUrlHandlerMapping
	4:RouterFunctionHandlerMapping
	
其实就是要根据request对象中的url,去所有的HandlerMapping中的映射关系中找对应的handler对象（基于注解@Controller的HandlerMethod）

SimpleUrlHandlerMapping BeanNameUrlHandlerMapping 是实例本身

不管是什么类型的HandlerMapping，都是不同的手段建立url和Handler的映射




# JSON参数解析和试图响应


```
@RequestBody 
@ResponseBody

RequestResponseBodyMethodProcessor


public class RequestMappingHandlerAdapter extends AbstractHandlerMethodAdapter
		implements BeanFactoryAware, InitializingBean {
			
			//消息转换器
			private List<HttpMessageConverter<?>> messageConverters;
			
			//这个set方法是什么时候被调用的，是在RequestMappingHandlerAdapter实例话的时候
			public void setMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
				this.messageConverters = messageConverters;
			}
			
				
			public List<HttpMessageConverter<?>> getMessageConverters() {
				return this.messageConverters;
			}
			
			//InitializingBean接口的应用
			@Override
			public void afterPropertiesSet() {
				// Do this first, it may add ResponseBody advice beans
				initControllerAdviceCache();
		
				if (this.argumentResolvers == null) {
					List<HandlerMethodArgumentResolver> resolvers = getDefaultArgumentResolvers();
					this.argumentResolvers = new HandlerMethodArgumentResolverComposite().addResolvers(resolvers);
				}
				if (this.initBinderArgumentResolvers == null) {
					List<HandlerMethodArgumentResolver> resolvers = getDefaultInitBinderArgumentResolvers();
					this.initBinderArgumentResolvers = new HandlerMethodArgumentResolverComposite().addResolvers(resolvers);
				}
				if (this.returnValueHandlers == null) {
					List<HandlerMethodReturnValueHandler> handlers = getDefaultReturnValueHandlers();
					this.returnValueHandlers = new HandlerMethodReturnValueHandlerComposite().addHandlers(handlers);
				}
			}
			
			private List<HandlerMethodArgumentResolver> getDefaultArgumentResolvers() {
					List<HandlerMethodArgumentResolver> resolvers = new ArrayList<>(30);
				//ResponseBody解析
				resolvers.add(new RequestResponseBodyMethodProcessor(getMessageConverters(), this.requestResponseBodyAdvice));
			}
}
```