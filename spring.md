BeanPostProcessor和Bean实例化初探

巩固的知识
    
	1:自定义注解扫描流程
	2:熟悉以后如何扩展（mybatis）
	
	
	
	
模板，观察，策略，委托（专人专事，不重要）


观察
   subject 
     -
	 -
	 -
	 -
   pubish------->obsver1
		 ------->obsver2
		 
		 123
          


课程内容
	1factoryMethod方法实例化
	2有参无参构造函数实例化
	3@Autowired @Value @Resource
	
	
	
	多个有参构造函数的理解
	
	
	3.7
		InstantiationAwareBeanPostProcessor 接口的使用
		注解方法和属性的收集，了解注解搜集类的大致结构
		
		//完成属性填充，ioc,di的核心方法
		//doCreateBean-4,填充属性的时候完成了循环依赖最后一步
		populateBean(beanName, mbd, instanceWrapper);
		/**
		 * 1 调用Aware接口
		 * 2 调用@PostConstruct注解的方法，这里是通过BeanPostProcessor实现的该功能
		 * 3 调用了实现InitiaLizingBean接口的类中的afterPropertiesSet方法调用initMethod中配置的方法
		 * 4 代理实例创建，用BeanPostProcessor接口来完成代理实例的生成
		 */
		exposedObject = initializeBean(beanName, exposedObject, mbd);
		
		
		@PostConstrust 例子
		
		补充ApplicationListener 中bean的添加
		
	3.8日
		单列循环依赖（允许）
		构造函数循环依赖（不行）
		多列循环依赖（不行）
	3.9日
		bean的销毁
		配置文件解析
		Enviroment对象
		@Value支撑
		@Lazy构造函数 实例化过程中没有出发第
		
		
		id  =class org.springframework.context.support.PropertySourcesPlaceholderConfigurer
		      class org.springframework.context.support.PropertySourcesPlaceholderConfigurer
			  
			 
		@Value 注入过程明天看
		@Lazy构造函数 实例化过程中没有出发第
	
	3.20日
		1factorybean接口入口
		2自定义scope
		3@PropertySource
		4@ComponentScan
		
		
		ConfigurationClassPostProcessor
			@PropertySource
			@ComonentScan
			@Bean
			@Import
			@ImporySource
			@Configuration
			
		理解metaData对象
	
	3.25日
		@Condition原理
		@Bean实现原理
		ImportBeanDefinitionRegister接口的调用
		Congiguration原理
			
	factorybean忘记了
			
	
	3.28日
		理解cglb代理（@Configuration 引出了cglb）
		@Di 代码（记得要看）
	
	4.3
	   动态代理
	   
	4.11
		1 aop 获取切面的过程 切面的排序
		2 生成代理的过程
		3 链式调用
	
	4.17
	    1代理提前生成 没什么用
		2scopeProxy
	
	4.21
	   巩固aop,找到几个match方法的钩子
	   明白aop代理的advice的数据结构，调用advice增强的基本逻辑
	   彻底搞清楚俩种切面的源码处理逻辑
	   before火炬传递
	
	5.29
	   取代web.xml用到的技术是tomcat的spi技术

# springBean生命周期
	.class--构造函数-----普通像----依赖注入-----初始化前(@PostConstruct)---初始化中---初始化后(aop)----代理对象----bean
	
	代理对象生成判断：1找出所有切面bean
					 2 遍历
					 3 遍历方法
					 4 方法是否与当前对象的方法匹配 
					 5 缓存 对象===》方法

	userServiceProxy--代理对象---代理对象.target=userService



    UserServiceProxy extends userService{
		userService target;
		public void aaa(){
			//执行@Before
			target.aaa();
		}
	}

	
