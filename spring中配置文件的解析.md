我们回忆一下xml配置文件
### 配置文件application.properties
```
url=http://www.baidu.com
```
### java类
```
@Data
public class DataSource {
	private String url;
}
```
### spring.xml
```
<context:property-placeholder location="classpath:application.properties"/>

<!--配置-->
<bean id="dataSource" class="com.lgb.test.properties.DataSource">
	<property name="url" value="${url}"/>
</bean>
```
这样我们就能将配置文件中的url赋值给我们DataSource类中的url

## 解析自定义标签property-placeholder
```
class PropertyPlaceholderBeanDefinitionParser extends AbstractPropertyLoadingBeanDefinitionParser {
	@Override
	@SuppressWarnings("deprecation")
	protected Class<?> getBeanClass(Element element) {
		
		if (SYSTEM_PROPERTIES_MODE_DEFAULT.equals(element.getAttribute(SYSTEM_PROPERTIES_MODE_ATTRIBUTE))) {
			//占位符解析类
			return PropertySourcesPlaceholderConfigurer.class;
		}
		return org.springframework.beans.factory.config.PropertyPlaceholderConfigurer.class;
	}
}
```
### 自定义标签解析 <context:property-placeholder >
### 
```
public abstract class AbstractApplicationContext extends DefaultResourceLoader
		implements ConfigurableApplicationContext {
	public void refresh() throws BeansException, IllegalStateException {
		synchronized (this.startupShutdownMonitor) {		
			/**
			 * 配置文件的解析
			 * 1创建beanfactory对象
			 * 2解析xml
			 *   传统标签解析 bean,import等
			 *	 自定义标签解析<context:component-scan base-package=""></>
			 *	 	自定义标签解析流程：
			 *	 		a:根据当前解析标签的头信息找到对象的namespaceUrl
			 *	 	    b:加载spring jar 中的spring.handlers文件，并建立映射关系
			 *	 	    c:根据namespaceUrl从映射关系中找到对应的实现了NameSpaceHandle的接口
			 *	 	    d:调用类的init方法，init方法定义了各种自定义标签的解析类
			 *	 	    e:根据namespaceUrl找到对应的解析类，然后调用paser完成标签解析
			 * 3解析出来的xml封装成对象（beanDefine）
			 *
			 * DefaultListableBeanFactory是ConfigurableListableBeanFactory的实现
			 */
			ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();
		}
	}
			
}
```
### obtainFreshBeanFactory()
这个方法完成了很多事情，主要时将spring bean对象解析成bd对象
```
protected ConfigurableListableBeanFactory obtainFreshBeanFactory() {
		//核心方法必须读5，模板设计模式
		/**
		 * AbstractApplicationContext 有如下俩个实现
		 * 		实现1： AbstractRefreshableApplicationContext
		 * 		实现2： GenericApplicationContext
		 *
		 * AnnotationConfigApplicationContext extends GenericApplicationContext
		 *
		 * ClassPathXmlApplicationContext
		 *
		 */
		refreshBeanFactory();
		return getBeanFactory();
}
```
### refreshBeanFactory()方法会调用到子类中
这个方法创建了DefaultListableBeanFactory，这个就是我们常说的bean工厂
 ```
这里是典型的模板设计模式
@Override
protected final void refreshBeanFactory() throws BeansException {
	//如果BeanFactory 不为空则清空beanFactory
	if (hasBeanFactory()) {
		destroyBeans();
		closeBeanFactory();
	}
	try {
		//beanFactory 实例工厂
		DefaultListableBeanFactory beanFactory = createBeanFactory();
		//是否可以循环依赖，是否允许相同名称重新注册不同的bean实现
		beanFactory.setSerializationId(getId());
		customizeBeanFactory(beanFactory);
		//解析XMl 将xml中的标签封装成beanDefinition这个方法很重要5
		loadBeanDefinitions(beanFactory);
		this.beanFactory = beanFactory;
	}
	catch (IOException ex) {
		throw new ApplicationContextException("I/O error parsing bean definition source for " + getDisplayName(), ex);
	}
}
```
### loadBeanDefinitions(beanFactory)
注意这个方法解析XMl 将xml中的标签封装成beanDefinition
```
public abstract class AbstractXmlApplicationContext {
	@Override
	protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) throws BeansException, IOException {
		// Create a new XmlBeanDefinitionReader for the given BeanFactory.
		//创建XML解析器，这里是一个委托模式（专人专事）
		XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);

		// Configure the bean definition reader with this context's
		// resource loading environment.
		beanDefinitionReader.setEnvironment(this.getEnvironment());
		//我们在入口处new ClassPathXmlApplicationContext("xxx.xml"),
		// 这个this是ClassPathXmlApplicationContext对象，ClassPathXmlApplicationContext 实现了 ApplicationContext
		// 实现了 ResourceLoader
		beanDefinitionReader.setResourceLoader(this);
		beanDefinitionReader.setEntityResolver(new ResourceEntityResolver(this));

		// Allow a subclass to provide custom initialization of the reader,
		// then proceed with actually loading the bean definitions.
		initBeanDefinitionReader(beanDefinitionReader);
		//这个方法很重要，上面的都是给beanDefinitionReader设置相关属性，
		// 这个方法才是真正解析xml文件的，重要程度5
		loadBeanDefinitions(beanDefinitionReader);
	}
}

//这个方法中拿到xml路劲信息然后调用 XmlBeanDefinitionReader中的loadBeanDefinitions();
protected void loadBeanDefinitions(XmlBeanDefinitionReader reader) throws BeansException, IOException {
		Resource[] configResources = getConfigResources();
		if (configResources != null) {
			reader.loadBeanDefinitions(configResources);
		}
		//解析xml路劲信息
		String[] configLocations = getConfigLocations();
		if (configLocations != null) {
			reader.loadBeanDefinitions(configLocations);
		}
}
```
### AbstractBeanDefinitionReader中的loadBeanDefinitions()
这里主要将配置文件通过流的方式解析成一个Resource数组对象，然后传给loadBeanDefinitions(resources)
```
public abstract class AbstractBeanDefinitionReader implements BeanDefinitionReader, EnvironmentCapable {
	public int loadBeanDefinitions(String location, @Nullable Set<Resource> actualResources) throws BeanDefinitionStoreException {
		ResourceLoader resourceLoader = getResourceLoader();

		if (resourceLoader instanceof ResourcePatternResolver) {
			// Resource pattern matching available.
			try {
				//通过流的方式加载xml文件
				Resource[] resources = ((ResourcePatternResolver) resourceLoader).getResources(location);
				//主要看这个方法，重要程度5
				int count = loadBeanDefinitions(resources);
				if (actualResources != null) {
					Collections.addAll(actualResources, resources);
				}
				if (logger.isTraceEnabled()) {
					logger.trace("Loaded " + count + " bean definitions from location pattern [" + location + "]");
				}
				return count;
			}
			catch (IOException ex) {
				throw new BeanDefinitionStoreException(
						"Could not resolve bean definition resource pattern [" + location + "]", ex);
			}
		}
	}
}
```
### 调用到XmlBeanDefinitionReader中的loadBeanDefinitions()
```
public int loadBeanDefinitions(EncodedResource encodedResource) throws BeanDefinitionStoreException {
	Assert.notNull(encodedResource, "EncodedResource must not be null");
	if (logger.isTraceEnabled()) {
		logger.trace("Loading XML bean definitions from " + encodedResource);
	}

	Set<EncodedResource> currentResources = this.resourcesCurrentlyBeingLoaded.get();

	if (!currentResources.add(encodedResource)) {
		throw new BeanDefinitionStoreException(
				"Detected cyclic loading of " + encodedResource + " - check your import definitions!");
	}
	//获取Resource对象中的xml文件流对象
	try (InputStream inputStream = encodedResource.getResource().getInputStream()) {
		//InputSource是jdk中的sax xml文件解析对象
		InputSource inputSource = new InputSource(inputStream);
		if (encodedResource.getEncoding() != null) {
			inputSource.setEncoding(encodedResource.getEncoding());
		}
		//主要看这个方法重要程度5
		return doLoadBeanDefinitions(inputSource, encodedResource.getResource());
	}
}
protected int doLoadBeanDefinitions(InputSource inputSource, Resource resource)
			throws BeanDefinitionStoreException {
	
	//包装成Document，jdk  API
	Document doc = doLoadDocument(inputSource, resource);
	//根据Document 对象封装成 beanDefinitions对象，重要程度5
	int count = registerBeanDefinitions(doc, resource);
	if (logger.isDebugEnabled()) {
		logger.debug("Loaded " + count + " bean definitions from " + resource);
	}
	return count;
	
}

public int registerBeanDefinitions(Document doc, Resource resource) throws BeanDefinitionStoreException {
		//这里又是委托实际模式 委托BeanDefinitionDocumentReader进行解析
		BeanDefinitionDocumentReader documentReader = createBeanDefinitionDocumentReader();
		int countBefore = getRegistry().getBeanDefinitionCount();
		//主要看这个方法     createReaderContext(resource),XmlReadContext上下文封装成了XmlBeanDefinition
		documentReader.registerBeanDefinitions(doc, createReaderContext(resource));
		return getRegistry().getBeanDefinitionCount() - countBefore;
}
```
### 来到这个方法，才是真正到了解析配置文件的时候，前面都是铺垫
前面是为了将xml文件变成流对象，然后经过一系列的包装最后变成Document对象。作为这一切才真正开始进行解析配置文件。

解析主要有俩种类型：默认标签解析，自定义标签即系

这里主要了解自定义标签解析delegate.parseCustomElement(ele)

```
protected void parseBeanDefinitions(Element root, BeanDefinitionParserDelegate delegate) {
	if (delegate.isDefaultNamespace(root)) {
		//拿到子元素
		NodeList nl = root.getChildNodes();
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			if (node instanceof Element) {
				Element ele = (Element) node;
				if (delegate.isDefaultNamespace(ele)) {
					//默认标签，很重要
					parseDefaultElement(ele, delegate);
				}
				else {
					//自定义标签解析，很重要
					delegate.parseCustomElement(ele);
				}
			}
		}
	}
	else {
		delegate.parseCustomElement(root);
	}
}
```